﻿var colorData = ["#46c80d", "#ffe404", "#ff398d", "#ff0000", "#0291ff"];

var Status;
(function (Status) {
    Status[Status["Ready"] = 0] = "Ready";
    Status[Status["Win"] = 1] = "Win";
    Status[Status["Unreceived"] = 2] = "Unreceived";
    Status[Status["Received"] = 4] = "Received";
    Status[Status["Received"] = 8] = "NotJoin";
})(Status || (Status = {}));

var WinType;
(function (WinType) {
    WinType[WinType["Scash1"] = 0] = "Scash1";
    WinType[WinType["Scash3"] = 1] = "Scash3";
    WinType[WinType["Scash5"] = 2] = "Scash5";
    WinType[WinType["Polaroid"] = 3] = "Polaroid";
    WinType[WinType["Camara"] = 4] = "Camara";
    WinType[WinType["Scash10"] = 5] = "Scash10";
    WinType[WinType["I6"] = 6] = "I6";
    WinType[WinType["Top1"] = 7] = "Top1";
    WinType[WinType["Other"] = 8] = "Other";
})(WinType || (WinType = {}));

function WinTypeCh(WinTypeValue) {
    var result = "";
    switch (WinTypeValue) {
        case 0:
            result = '17Life紅利金$1000';
            break;
        case 1:
            result = '17Life紅利金$3000';
            break;
        case 2:
            result = '17Life紅利金$5000';
            break;
        case 3:
            result = '拍立得';
            break;
        case 4:
            result = '類單';
            break;
        case 5:
            result = '17Life紅利金$10000';
            break;
        case 6:
            result = 'IPHONE6';
            break;
        case 7:
            result = '綜合獎金$30000';
            break;
        case 8:
            result = '其他';
            break;
    }
    return result;
}

var winnerModel = (function () {
    function winnerModel(i, n, _s, _w) {
        this.id = i;
        this.numberSize = n || 50;
        this._status = _s || 0 /* Ready */;
        this._winType = _w || 0 /* Ready */;
    }
    
    winnerModel.prototype.getNumber = function () {
        return ((this.id % this.numberSize) + 1);
    };
    winnerModel.prototype.getWinAngle = function () {
        return 'rotate(-' + ((((Math.random() * 0.6) + 0.2) + win.getNumber() - 1) * 7.2) + 'deg)';
    };
    winnerModel.prototype.getColor = function () {
        return (this.id / this.numberSize) | 0;
    };
    winnerModel.prototype.getWinerListString = function () {
        //var isValueProperty = parseInt(enumMember, 10) >= 0
        //if (isValueProperty) {
        //    console.log("enum member: ", myEnum[enumMember]);
        //}
        var data = winners.filter(function (x) { return x.status > 0 /* Ready */; })
        .sort(function (n1, n2) { return n1.winType - n2.winType; });
        var result = '';
        var tmpWinType = -1;
        for (var i in data) {
            var winHtml = '';
            
            var item = data[i];
            var wType = WinTypeCh(item.winType).split('$');
            if (tmpWinType != item.winType) {
                winHtml += '<div class="winner-scroll-text">' +
                '<h1>' + wType[0] + '</h1>' +
                ((wType[1]==undefined)?'':'<h1>' + wType[1] + '</h1>') +
                '</div>';
            }
            tmpWinType = item.winType;
            //winHtml += data[i].getNumber() + data[i].getColor();
            winHtml += '<div class="winnerFinal">' +
                '<span style="background:' + colorData[data[i].getColor()] + ' ;padding:10px;">' + 
                data[i].getNumber() + '</span>' +
                '</div>';

            result += winHtml;
        }
        //console.log(result);
        return result;
    };
    Object.defineProperty(winnerModel.prototype, "status", {
        get: function () {
            return this._status;
        },
        set: function (s) {
            this._status = s;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(winnerModel.prototype, "winType", {
        get: function () {
            return this._winType;
        },
        set: function (s) {
            this._winType = s;
        },
        enumerable: true,
        configurable: true
    });
    return winnerModel;
})();

var finalModel = (function () {
    function finalModel(num, color) {
        this._num = num;
        this._color = color;
    }
    finalModel.prototype.getFinalHtmlStr = function () {
        var result =
 '<div class="makeWinnerInfo winnerFinal" >' +
            '<span style="background:' + this._color + '" > ' +
            this._num + ' </span ></div>' +
            '<div class="makeWinnerAction borderSize" style = "display:none" >' +
            '<button>get </button >' +
            '<button>no </button >' +
            '</div>';
        return result;
    };
    
    finalModel.prototype.getFinalHtmlSetting = function () {
        var result =
 '<div class="makeWinnerInfo winnerFinal" >' +
            '<span style="background:' + this._color + '" > ' +
            this._num + ' </span ></div>' +
            '<div class="makeWinnerAction borderSize" style = "display:none" >' +
            '<button>get </button >' +
            '<button>no </button >' +
            '</div>';
        return result;
    };
    return finalModel;
})();


function CustomAlert() {
    this.render = function (dialog) {
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH + "px";
        dialogoverlay.addEventListener('click', function () { Alert.ok() });
        dialogbox.style.left = (winW / 2) - (450 * .5) + "px";
        dialogbox.style.top = "100px";
        dialogbox.style.display = "block";
        dialogbox.addEventListener('click', function () { Alert.ok() });
        document.getElementById('dialogboxbody').innerHTML = dialog;
    }
    this.ok = function () {
        document.getElementById('dialogbox').style.display = "none";
        document.getElementById('dialogoverlay').style.display = "none";
    }
}
var Alert = new CustomAlert();