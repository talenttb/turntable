﻿
function buildView() {
    'use strict';
    //////////////////////////////////////////////////////////////////////////////////
    var canvasWidth = 440,
        canvasHeight = 440,
        outerRadius = 185,
        innerRadius = 0,
        color = d3.scale.category20();
    
    var dataValue = d3.range(1, 51);
    var data = [];
    for (var i = 0; i < 50;) {
        var num = (i / 10) | 0;
        data.push({
            "data": 1,
            "text": ++i,
            "color": colorData[num]
        });
    }
    
    var svg = d3.select("#turntableDiv")
        .append("svg:svg")
        .attr("width", canvasWidth)
        .attr("height", canvasHeight);
    
    
    
    var vis = svg
        .append("svg:g")
        .data([data])
        .attr("id", "turntable")
        .attr("width", canvasWidth)
        .attr("height", canvasHeight)
        .attr('class', 'make-center')
        .append("svg:g")
        .attr("transform", "translate(" + canvasWidth / 2 + "," + canvasHeight / 2 + ")");
    
    var arc = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius);
    
    var pie = d3.layout.pie()
        .value(function (d) {
        return d.data;
    });
    
    var arcs = vis.selectAll("g.slice")
        .data(pie)
        .enter()
        .append("svg:g")
        .attr("class", "slice");
    
    var transText = d3.svg.arc().outerRadius(outerRadius);
    arcs.append("svg:text")
        .attr("transform", function (d) {
        d.outerRadius = outerRadius + 55;
        d.innerRadius = outerRadius + 45;
        return "translate(" + transText.centroid(d)[0] + "," + ((transText.centroid(d)[1] | 0) + 8) + ")";
    })
        .attr("text-anchor", "middle")
        .style("fill", "#000")
        .style("font", "bold 20px Arial")
        .text(function (d, i) {
        return ++i;
    });
    
    //------
    var visColors = svg
        .append("svg:g")
        .data([data])
        .attr("id", "turntableColors")
        .attr("width", canvasWidth)
        .attr("height", canvasHeight)
        .attr('class', 'make-center')
        .append("svg:g")
        .attr("transform", "translate(" + canvasWidth / 2 + "," + canvasHeight / 2 + ")");
    
    var arcColors = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius);
    
    var pieColors = d3.layout.pie()
        .value(function (d) {
        return d.data;
    });
    
    var arcsColors = visColors.selectAll("g.slice")
        .data(pieColors)
        .enter()
        .append("svg:g")
        .attr("class", "slice");
    
    arcsColors.append("svg:path")
        .attr("fill", function (d, i) {
        return data[i].color;
    })
        .attr("d", arcColors);
    
    //------------------------------------
    
    //Draw the line
    var line = svg.append("line")
        .attr("x1", canvasWidth / 2)
        .attr("y1", canvasHeight / 100)
        .attr("x2", canvasWidth / 2)
        .attr("y2", canvasHeight / 3)
        .attr("stroke-width", 3)
        .attr("stroke", "red");
    
    //////////////////////////////////////////////////////////////////////////////////
    
    //chrome bug fix
    $($('#turntable').find('.slice text')[25]).text("1");
    $($('#turntable').find('.slice text')[1]).text("26");
    $($('#turntable').find('.slice text')[0]).text("2");
    
    $($('#turntableColors').find('.slice path')[25]).css("fill", "#46c80d");
    $($('#turntableColors').find('.slice path')[1]).css("fill", "#ff398d");
    //$($('#turntableColors').find('.slice path')[0]).css("fill", "#ffe404");
}

var turntableStop = false;
var winner = false;
var rdStop = [false, false, false, false, false, false, false, false];
var playing = false;
function addEvent(doc) {
    doc.getElementById('stopBtn').addEventListener('click', function () {
        var winSelectType = doc.getElementById("sel1").selectedIndex;
        if (0 <= winSelectType && winSelectType < 2) {
            var i = 0;
            if (playing) {
                var stepToStop = setInterval(function () {
                    if (i > 7) {
                        clearInterval(stepToStop);
                    } else {
                        rdStop[i] = true;
                        i++;
                    }
                }, 1000);
                playing = false;
            }
        }
        //else if (2 <= winSelectType && winSelectType < 3) {
        //    turntableStop = true;
        //}
        else if (2 <= winSelectType && winSelectType < 9) {
            turntableStop = true;
        }
    }, false);
    
    doc.getElementById('playBtn').addEventListener('click', function () {
        var winSelectType = doc.getElementById("sel1").selectedIndex;
        resetStatus();
        if (0 <= winSelectType && winSelectType < 2) {
            
            var rdmList = $('.rdmNumber');
            
            
            if (!playing) {
                
                var i = 0;
                var stepToStop = setInterval(function () {
                    if (i > 7) {
                        clearInterval(stepToStop);
                    } else {
                        animationTime(rdmList[i], i);
                        i++;
                    }
                }, 100);
                playing = true;
            }
        }
        else if (2 <= winSelectType && winSelectType < 9) {
            
            turning();
        }
    }, false);
    
    doc.getElementById('top-Menu').addEventListener('click', function (e) {
        //topMenuClick(doc);
    }, false);
    
    doc.getElementById('sel1').addEventListener('change', function (e) {
        var x = this.selectedIndex;
        
        if (0 <= x && x < 2) { showTurntable(false); }
        //else if (2 <= x && x < 3) { showTurntable(false); }
        else if (2 <= x && x < 9) { showTurntable(true); }

    }, false);
    
    //$('#finalWinner p').on('click', function () {
    //    location.href = window.location.pathname.replace('index', 'result');
    //});
    
    addOption(doc.getElementById('sel1'));
    
    function addOption(el) {
        for (var i = 0; i < 9; i++) {
            var op = document.createElement("option");
            op.value = i;
            op.text = WinTypeCh(i);
            el.options.add(op, i);
        }
    }
    
    function topMenuClick(doc) {
        var left_Col = doc.getElementById('left-Col');
        if (left_Col.classList.contains('notShow')) {
            left_Col.classList.remove('notShow');
        } else {
            left_Col.classList.add('notShow');
        }
        
        var rdmSection = doc.getElementById('rdmSection');
        if (rdmSection.classList.contains('notShow')) {
            rdmSection.classList.remove('notShow');
        } else {
            rdmSection.classList.add('notShow');
        }
    }
    
    function showTurntable(b) {
        var rdmSection = doc.getElementById('rdmSection');
        var left_Col = doc.getElementById('left-Col');
        if (b) {
            rdmSection.classList.add('notShow');
            left_Col.classList.remove('notShow');
        } else {
            left_Col.classList.add('notShow');
            rdmSection.classList.remove('notShow');
        }
    }
}

function resetStatus() {
    rdStop = [false, false, false, false, false, false, false, false];
    turntableStop = false;
    winner = false;
}

var turning = function () {
    $('#turntable,#turntableColors').playKeyframe({
        name: 'rototeCircle',
        duration: '1s',
        timingFunction: 'linear',
        delay: '0s',
        iterationCount: 'infinite',
        direction: 'normal',
        fillMode: 'backwards',
    }, function () {
        if (!turntableStop) {
            turning();
        } else {
            if (!winner) {
                winner = true;
                win = getWinner();
                
                $.keyframe.define({
                    name: 'winnerNumber' + win.getNumber(),
                    from: {
                        'transform': 'rotate(0deg)'
                    },
                    to: {
                        'transform': win.getWinAngle()
                    }
                });
                $('#turntable').resetKeyframe(function () {
                    $('#turntable').playKeyframe({
                        name: 'winnerNumber' + win.getNumber(),
                        duration: '2.5s',
                        //timingFunction: 'cubic-bezier(.15,.51,.07,.93)',
                        timingFunction: 'cubic-bezier(0, 0, .13, 1)',
                        delay: 0,
                        iterationCount: '1',
                        direction: 'normal',
                        fillMode: 'forwards',
                    });
                });
                
                $.keyframe.define({
                    name: 'winnerColor' + win.getColor(),
                    from: {
                        'transform': 'rotate(0deg)'
                    },
                    to: {
                        'transform': 'rotate(' +
                            (-((((Math.random() * 0.8) + 0.2) + win.getColor()) * 72)) + 'deg)'
                    }
                });
                $('#turntableColors').resetKeyframe(function () {
                    $('#turntableColors').playKeyframe({
                        name: 'winnerColor' + win.getColor(),
                        duration: '2.5s',
                        //timingFunction: 'cubic-bezier(.15, .51, .07, .93)',
                        timingFunction: 'cubic-bezier(0, 0, .13, 1)',
                        //timingFunction: 'ease',
                        delay: 0,
                        iterationCount: '1',
                        direction: 'normal',
                        fillMode: 'forwards',
                    });
                });
                
                //var finalWinnerModel = (new finalModel(win.getNumber(), colorData[win.getColor()].color)).getFinalHtmlStr();
                setTimeout(function () {
                    var html = '<div class="winnerHead">得獎人是?</div><div class="winnerBody">' +
                '<span style="background:' + colorData[win.getColor()] + ';padding:100px;">' + 
                win.getNumber() + '</span>' +
                '</div>';
                    Alert.render(html);

                    document.getElementsByTagName('marquee')[0].innerHTML = win.getWinerListString();
                }, 2900);
            }
        }
    });
};

function getWinner() {
    var winner = -1;
    var counter = 0;
    while (counter<=250) {
        counter++;
        var r = (Math.random() * 250) | 0;
        if (winners[r].status == Status.Ready) {
            winner = r;
            break;
        }
    }
    if (winner == -1) {
        alert('All of us are winner');
        return false;
    }
    winners[winner].status = Status.Win;    
    var idx = document.getElementById("sel1").selectedIndex;
    //var ops = document.getElementById("sel1").options;
    //alert("Index: " + y[x].index + " is " + y[x].text);
    winners[winner].winType = idx;
    localStorage.setItem('winner', JSON.stringify(winners));
    return winners[winner];
}
var winners = [];
$(function () {
    buildView();
    addEvent(document);
    
    var isInit = true;
    
    if (localStorage.getItem('winner') === null || localStorage.getItem('winner') === '') {
        console.log('winner is clear');
    } else {
        if (confirm('reset？') == true) {
            localStorage.winner = '';
        } else {
            isInit = false;
        }
    }
    
    if (isInit) {
        for (var i = 0; i <= 250; i++) {
            winners.push(new winnerModel(i));
        }
        localStorage.setItem('winner', JSON.stringify(winners));
        winners.length = 0;
    }
    
    var data = JSON.parse(localStorage.getItem('winner'));
    
    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        winners.push(new winnerModel(item.id, item.numberSize, item._status,item._winType));

        if (i == data.length-1) { 
            document.getElementsByTagName('marquee')[0].innerHTML = 
            new winnerModel(item.id, item.numberSize, item._status, item._winType).getWinerListString();
        }
    }
});

var animationTime = function (obj, id) {
    var rdmNumberColor = function () {
        if (rdStop[id]) {
            clearInterval(window['rdm' + id]);
            window['rdm' + id] = null;
            var win = getWinner();
            
            $(obj).find('.randomFinal').text(win.getNumber());
            $(obj).find('.randomColor').css("background", colorData[win.getColor()]);
            
            document.getElementsByTagName('marquee')[0].innerHTML = win.getWinerListString();

            //var finalWinnerModel = (new finalModel(win.getNumber(), colorData[win.getColor()])).getFinalHtmlStr();
            //$(finalWinnerModel).insertAfter($('#finalWinner p'));
        } else {
            $(obj).find('.randomFinal').text(
                Math.floor(Math.random() * 5) +
                Math.floor(Math.random() * 10)
            );
            
            var c = colorData[(Math.random() * 5) | 0];
            $(obj).find('.randomColor').css("background", c);
        }
    };
    
    window['rdm' + id] = setInterval(function () {
        rdmNumberColor();
    }, 100);
};
